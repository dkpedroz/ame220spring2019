# Assignment 1

**Due Jan 23<sup>rd</sup> before class**

## Task

Write a simple single page resume (in html). It should have at-least 4 sections

0. Name + thumbnail (phone, address and email are links)
1. Objective
2. Education (use table)
3. Experience (use one paragraph for each entry -- ordered list)
4. References (unordered list)

## Grading

1. Section formatting.
2. Use of table, image, hyperlinks and lists.
